# M7A Add-ons for Blender

## Morphing Images
Create morphing between images
### Download
- [ ] [Morphing Images v2.0](https://gitlab.com/3DMish/m7a-addons/-/raw/master/morphing-images/m7a_morph_v2.py?inline=false)
- [ ] [OLD] [~~3DM Morph v0.1.5~~](https://gitlab.com/3DMish/m7a-addons/-/raw/master/morphing-images/old_versions/3dm_morph.py?inline=false)

<details><summary>ScreenShots</summary>
<img src="https://gitlab.com/3DMish/m7a-addons/-/raw/master/morphing-images/screenshot_1.png" />
<img src="https://gitlab.com/3DMish/m7a-addons/-/raw/master/morphing-images/screenshot_2.png" />
</details>
